/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chanapp.utilitaires;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.swing.text.html.HTML;

/**
 *
 * @author Stanley
 */
public class MesOutils {
    
    public void changePage(String path, String title) throws IOException{
      
        Parent root = FXMLLoader.load(getClass().getResource(path));
        
        Scene scene = new Scene(root);
        
        Stage stage = new Stage();
        stage.setTitle(title);
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }
    
    public void changePageAndHide(String path, String title, Event event) throws IOException{
        ((Node) event.getSource()).getScene().getWindow().hide();
        System.out.println(path);
        Parent root = FXMLLoader.load(getClass().getResource(path));

        Scene scene = new Scene(root);
        String data = "test";
        Stage stage = new Stage();
        stage.initOwner(((Node) event.getSource()).getScene().getWindow());
        stage.setTitle(title);
        stage.setScene(scene);
        stage.setUserData(data);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }
    
    public static LocalDate convertToLocalDate(Date date){
    
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }
    
    public static String toHash(String text) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] msgDigest = md.digest(text.getBytes());
            BigInteger bi = new BigInteger(1, msgDigest);
            
            String hash = bi.toString(16);
            while (hash.length()<32) {                
                hash = "0"+hash;
            }
            return hash;
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }
        
    }
}
