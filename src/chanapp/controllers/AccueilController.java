/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chanapp.controllers;

import chanapp.utilitaires.MesOutils;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author USER
 */
public class AccueilController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void goToConnexion(MouseEvent event) throws IOException{
        new MesOutils().changePageAndHide("/chanapp/vues/Connexion.fxml", "CHAN2021",event);
    }
    
    public void goToCalendrier(ActionEvent event) throws IOException{
        new MesOutils().changePage("/chanapp/vues/CalendrierMatchs.fxml", "CHAN2021");
    }
    public void goToMatchEnCours(ActionEvent event) throws IOException{
        new MesOutils().changePage("/chanapp/vues/MatchEnCours.fxml", "CHAN2021");
    }
    public void goToTousLesMatchs(ActionEvent event) throws IOException{
        new MesOutils().changePage("/chanapp/vues/ListeMatchs.fxml", "CHAN2021");
    }
    public void goToMesMatchs(ActionEvent event) throws IOException{
        new MesOutils().changePage("/chanapp/vues/MesMatchs.fxml", "CHAN2021");
    }
    public void goToToutesLesEquipes(ActionEvent event) throws IOException{
        new MesOutils().changePage("/chanapp/vues/ToutesLesEquipes.fxml", "CHAN2021");
    }
    public void goToMesEquipes(ActionEvent event) throws IOException{
        new MesOutils().changePage("/chanapp/vues/MesEquipes.fxml", "CHAN2021");
    }
    public void goToPhaseDePools(ActionEvent event) throws IOException{
        new MesOutils().changePage("/chanapp/vues/Pools.fxml", "CHAN2021");
    }
    public void goToEliminationDirect(ActionEvent event) throws IOException{
        System.out.println("ok");
    }
    public void goToCommande(ActionEvent event) throws IOException{
        new MesOutils().changePage("/chanapp/vues/MesCommandes.fxml", "CHAN2021");
    }
    public void goToConsuterProfile(ActionEvent event) throws IOException{
        new MesOutils().changePage("/chanapp/vues/ModificationCompte.fxml", "CHAN2021");
    }
}
