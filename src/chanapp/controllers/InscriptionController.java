/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chanapp.controllers;

import chanapp.models.Fan;
import chanapp.utilitaires.MesOutils;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author USER
 */
public class InscriptionController implements Initializable {

    @FXML
    private TextField nomF;

    @FXML
    private TextField prenomF;

    @FXML
    private TextField ageF;

    @FXML
    private TextField nationaliteF;

    @FXML
    private TextField telF;

    @FXML
    private TextField mailF;

    @FXML
    private PasswordField mdpF;

    @FXML
    private PasswordField confirmF;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    public void goToConnexion(Event event) throws IOException{
        new MesOutils().changePageAndHide("/chanapp/vues/Connexion.fxml", "CHAN2021",event);
         System.out.println("12");
    }
    
    public void enregistrerFan(Event event) throws IOException{
        String mail = mailF.getText();
        int tel;
        
        String mdp = MesOutils.toHash(mdpF.getText());
        String nom = nomF.getText();
        String prenom = prenomF.getText();
        int Age;
        
        String nationalite = nationaliteF.getText();
        
        if (mail.equals("") || telF.getText().equals("") || mdpF.getText().equals("") || nom.equals("") || prenom.equals("") || ageF.equals("") || nationalite.equals("") ) {
            System.out.println("Entrer toutes les informations");
            return;
        }
        System.out.println(mdp);
        tel = Integer.parseInt(telF.getText());
        Age = Integer.parseInt(ageF.getText());
        if (mdpF.getText().equals(confirmF.getText())) {
            Fan fan = new Fan(mail, tel, mdp, nom, prenom, Age, nationalite);
            Fan.enregistrer(fan);
            new MesOutils().changePageAndHide("/chanapp/vues/Connexion.fxml", "CHAN2021",event);
            
        }else{
            System.out.println("les mot de passe ne corresponde pas");
            return;
        }
        
        
    }
}
