/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chanapp.controllers;

import chanapp.models.Equipe;
import chanapp.models.Match;
import chanapp.utilitaires.MesOutils;
import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author toshiba
 */
public class CalendrierMatchsController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    public TableView calendrier_table;
    @FXML 
    public TableColumn eq1Column;
    @FXML 
    public TableColumn lieuColumn;
    @FXML
    public TableColumn eq2Column;
    @FXML
    public TableColumn dateColumn;
    @FXML
    public TableColumn consulterColumn;
    @FXML
    public TableColumn commanderColumn;
    
    ObservableList<Match> list_matchs = FXCollections.observableArrayList();
    
    public void chargerMatch(){
        list_matchs.clear();
        Match.getMatchs().forEach((match) -> {
            Button consulter = new Button("consulter");
            Button commander = new Button("Commander ticket");
            consulter.setUserData(match.getIdMatch());
            commander.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    try {
                        new MesOutils().changePage("/chanapp/vues/CommandeTicket.fxml", "Commander Ticket");
                    } catch (IOException ex) {
                        System.out.println(ex);
                    }
                }
            });
            consulter.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    try {
                        new MesOutils().changePage("/chanapp/vues/ConsultationMatch.fxml", "Consulter match");
                    } catch (IOException ex) {
                        System.out.println(ex);
                    }
                }
            });
            match.setConsulter(consulter);
            match.setCommander(commander);
            
            list_matchs.add(match);
        });
    }
    
    public Date convertToDate(LocalDate ld){
        return Date.from(ld.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        
        
        eq1Column.setCellValueFactory(new PropertyValueFactory("nomEquipe1"));
        eq2Column.setCellValueFactory(new PropertyValueFactory("nomEquipe2"));
        dateColumn.setCellValueFactory(new PropertyValueFactory("dateDebut"));
        consulterColumn.setCellValueFactory(new PropertyValueFactory("consulter"));
        lieuColumn.setCellValueFactory(new PropertyValueFactory("lieu"));
        commanderColumn.setCellValueFactory(new PropertyValueFactory("commander"));
        
        
        chargerMatch();
        
        
        
        calendrier_table.setItems(list_matchs);
        
        
    }    
    


}

