/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chanapp.controllers;

import chanapp.models.Fan;
import chanapp.utilitaires.MesOutils;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author USER
 */
public class ConnexionController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TextField mailF;

    @FXML
    private TextField mdpF;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void goToInscription(MouseEvent event) throws IOException{
        new MesOutils().changePageAndHide("/chanapp/vues/Inscription.fxml", "CHAN2021",event);
    
    }
    
    public void goToAccueil(ActionEvent event) throws IOException{
//        String mail = mailF.getText();
//        String mdp = MesOutils.toHash(mdpF.getText());
//        
//        
//        if (Fan.exist(mail, mdp)) {
            new MesOutils().changePageAndHide("/chanapp/vues/Accueil.fxml", "CHAN2021",event);        
//        }else{
//            System.out.println("Information incorrect");
//        }
    }
    
}
