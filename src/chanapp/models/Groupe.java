/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chanapp.models;

import chanapp.ChanDatabase;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Stanley
 */
public class Groupe {
    
    public static ObservableList<Groupe> groupes = FXCollections.observableArrayList();
    
    private String nomGroupe;
    private int idGroupe;
    public Groupe(int idGroupe, String nomGroupe) {
        this.nomGroupe = nomGroupe;
        this.idGroupe = idGroupe;
    }

    public int getIdGroupe() {
        return idGroupe;
    }
    
    public String getNomGroupe() {
        return nomGroupe;
    }

    public void setNomGroupe(String nomGroupe) {
        this.nomGroupe = nomGroupe;
    }
    
    public static ObservableList<Groupe> getGroupe(){
        
        groupes.clear();
        
        ResultSet resultSet = ChanDatabase.select("SELECT * FROM groupes");
        try {
            while (resultSet.next()) {
                groupes.add(new Groupe(resultSet.getInt("idgroupe"), resultSet.getString("nomgroupe")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Groupe.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       return groupes;
    }   
    public void deleteGroupe(){
    }
    

    
    public void updateGroupe(){
        
    }
}
