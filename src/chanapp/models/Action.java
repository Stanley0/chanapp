/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chanapp.models;

import chanapp.ChanDatabase;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Stanley
 */
public class Action {
    private String evenement;
    private int chrono;
    private int idaction,idmacth,idjoueur;
    private String nomEquipe1,nomEquipe2,nomjoueur;
    
    public Action(String evenement, int chrono, int idmacth, int idjoueur) {
        this.evenement = evenement;
        this.chrono = chrono;
        this.idmacth = idmacth;
        this.idjoueur = idjoueur;
    }

    public Action(String evenement, int chrono, int idaction, int idmacth, int idjoueur) {
        this.evenement = evenement;
        this.chrono = chrono;
        this.idaction = idaction;
        this.idmacth = idmacth;
        this.idjoueur = idjoueur;
    }

    public String getNomEquipe1() {
        return nomEquipe1;
    }

    public String getNomEquipe2() {
        return nomEquipe2;
    }

    public String getNomjoueur() {
        return nomjoueur;
    }

    public void setNomEquipe1(String nomEquipe1) {
        this.nomEquipe1 = nomEquipe1;
    }

    public void setNomEquipe2(String nomEquipe2) {
        this.nomEquipe2 = nomEquipe2;
    }

    public void setNomjoueur(String nomjoueur) {
        this.nomjoueur = nomjoueur;
    }
    

    public int getIdaction() {
        return idaction;
    }

    public int getIdjoueur() {
        return idjoueur;
    }

    public int getIdmacth() {
        return idmacth;
    }
    
    
    public int getChrono() {
        return chrono;
    }

    public String getEvenement() {
        return evenement;
    }

    public void setChrono(int chrono) {
        this.chrono = chrono;
    }

    public void setEvenement(String evenement) {
        this.evenement = evenement;
    }
    
    public static void creer(Action action){
        String sql = "INSERT INTO `actions`(`idaction`, `idmacth`, `idjoueur`, `evenement`, `chrono`) "
                + "VALUES (NULL,'"+action.getIdmacth()+"','"+action.getIdjoueur()+"','"+action.getEvenement()+"','"+action.chrono+"')";
        ChanDatabase.insert(sql);
    }
    
    public static ArrayList<Action> getActions(){
        ArrayList<Action> actions = new ArrayList<Action>();
        
        String sql = "SELECT * FROM `actions`";
        ResultSet rs = ChanDatabase.select(sql);
        
        try {
            while (rs.next()) {                
                actions.add(new Action(rs.getString("evenement"), rs.getInt("chrono"), rs.getInt("idaction"), rs.getInt("idmacth"), rs.getInt("idjoueur")));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        
        for (Action action : actions) {
            sql = "SELECT * FROM jouer WHERE idmacth="+action.getIdmacth();
            rs = ChanDatabase.select(sql);
            try {
                while (rs.next()) {            
                    ResultSet rs1 = ChanDatabase.select("SELECT * FROM `equipes` WHERE idequipe='"+rs.getInt("idequipe1")+"'");
                    while (rs1.next()) {                        
                        action.setNomEquipe1(rs1.getString(3));
                    }
                    ResultSet rs0 = ChanDatabase.select("SELECT * FROM `equipes` WHERE idequipe='"+rs.getInt("idequipe2")+"'");
                    while (rs0.next()) {                        
                        action.setNomEquipe2(rs0.getString(3));
                    }
                }     
            } catch (Exception e) {
                System.out.println(e);
            }
            sql = "SELECT nom,prenom FROM joueurs WHERE idjoueur="+action.getIdjoueur();
            rs = ChanDatabase.select(sql);
            try {
                while (rs.next()) {                    
                    action.setNomjoueur(rs.getString("nom") + " " + rs.getString("prenom"));
                }
            } catch (Exception e) {
            }
        }
        
        return actions;
            
    }
    
    
}
