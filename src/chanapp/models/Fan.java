/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chanapp.models;

import chanapp.ChanDatabase;
import static chanapp.models.Equipe.equipes;
import chanapp.utilitaires.MesOutils;
import java.sql.ResultSet;

/**
 *
 * @author Stanley
 */
public class Fan extends  Personne{
    private String email;
    private int tel;
    private String mdp;
    private int idFan;

    public Fan(String email, int tel, String mdp, String nom, String prenom, int age, String nationalite) {
        super(nom, prenom, age, nationalite);
        this.email = email;
        this.tel = tel;
        this.mdp = mdp;
        
    }

    public Fan(int idFan, String nom, String prenom, int age, String nationalite, String email, int tel, String mdp) {
        super(nom, prenom, age, nationalite);
        this.email = email;
        this.tel = tel;
        this.mdp = mdp;
        this.idFan = idFan;
    }
    
    

    public String getEmail() {
        return email;
    }

    public String getMdp() {
        return mdp;
    }

    public int getTel() {
        return tel;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public void setTel(int tel) {
        this.tel = tel;
        
    }

    public int getIdFan() {
        return idFan;
    }
    
    public static void enregistrer(Fan fan){
        String sql = "INSERT INTO `fans`(`idfan`, `email`, `tel`, `mdp`, `nom`, `prenom`, `age`, `nationalite`) "
                + "VALUES (NULL,'"+fan.getEmail()+"','"+fan.getTel()+"','"+fan.getMdp()+"','"+fan.getNom()+"','"+fan.getPrenom()+"','"+fan.getAge()+"','"+fan.getNationalite()+"')";
        ChanDatabase.insert(sql);
        
    }
    
    public static boolean exist(String mail, String mdp) {
        String sql = "SELECT `email`, `mdp` FROM `fans`";
        ResultSet resultSet = ChanDatabase.select(sql);
        boolean b = false;
        try {
            while (resultSet.next()) { 
                System.out.println(resultSet.getString("mdp"));
                System.out.println(mdp);//
                if (resultSet.getString("email").equals(mail) && resultSet.getString("mdp").equals(mdp)) {
                    b = true;
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        
        return b;
    }
    
    public void modifierCompte (Fan fan){
    }
    
    public void supprimerCompte(int idFan){
    }
    
    public void deconnecter(){
    }
    public void connection (){
    }
    
    
    
}
