/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chanapp.models;

import chanapp.ChanDatabase;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;

/**
 *
 * @author Stanley
 */
public class Match {

    public static ObservableList<Match> matchs = FXCollections.observableArrayList();

    
    private int idMatch;
    private int scoreEq1;
    private int scoreEq2;
    private int idEq1;
    private int idEq2;
    private Date dateDebut;
    private String nomEquipe1;
    private String status;
    private String nomEquipe2;
    private Button consulter;
    private Button commander;
    private Date dateFin;
    private String lieu;

    public Match(int scoreEq1, int scoreEq2, Date dateDebut, Date dateFin, String lieu) {
        this.scoreEq1 = scoreEq1;
        this.scoreEq2 = scoreEq2;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.lieu = lieu;
    }

    public Match(int idMatch, int scoreEq1, int scoreEq2, Date dateDebut, Date dateFin, String lieu) {
        this.idMatch = idMatch;
        this.scoreEq1 = scoreEq1;
        this.scoreEq2 = scoreEq2;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.lieu = lieu;
    }
    public void setConsulter(Button consulter){
        this.consulter = consulter;
    }
    public Button getConsulter(){
        return consulter;
    }

    public Button getCommander() {
        return commander;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    
    public void setCommander(Button commander) {
        this.commander = commander;
    }
    
    

    public int getIdMatch() {
        return idMatch;
    }

    public String getNomEquipe1() {
        return nomEquipe1;
    }

    public String getNomEquipe2() {
        return nomEquipe2;
    }

    public void setNomEquipe1(String nomEquipe1) {
        this.nomEquipe1 = nomEquipe1;
    }

    public void setIdEq1(int idEq1) {
        this.idEq1 = idEq1;
    }

    public void setIdEq2(int idEq2) {
        this.idEq2 = idEq2;
    }

    public int getIdEq1() {
        return idEq1;
    }

    public int getIdEq2() {
        return idEq2;
    }
    
    

    public void setNomEquipe2(String nomEquipe2) {
        this.nomEquipe2 = nomEquipe2;
    }
    
    

    public Date getDateDebut() {
        return dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public String getLieu() {
        return lieu;
    }

    public int getScoreEq1() {
        return scoreEq1;
    }

    public int getScoreEq2() {
        return scoreEq2;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public void setScoreEq1(int scoreEq1) {
        this.scoreEq1 = scoreEq1;
    }

    public void setScoreEq2(int scoreEq2) {
        this.scoreEq2 = scoreEq2;
    }

    public static void Enregistrer(int idEq1,int idEq2,LocalDate datedebut, String lieu){
        String sql = "INSERT INTO `matchs`(`idmacth`, `scoreequ1`, `scoreequ2`, `datefin`, `datedebut`, `lieu`, `status`) "
                + "VALUES (NULL,NULL,NULL,NULL,'"+datedebut.toString()+"','"+lieu+"')";
        ChanDatabase.insert(sql);
        
        sql = "SELECT * FROM matchs";
        ResultSet rs = ChanDatabase.select(sql);
        int lastID = 0;
        try {
            while (rs.next()) {            
                lastID = rs.getInt("idmacth");
            }     
        } catch (Exception e) {
            System.out.println(e);
        }
        sql = "INSERT INTO `jouer`(`idequipe1`, `idmacth`, `idequipe2`) VALUES ('"+idEq1+"','"+lastID+"','"+idEq2+"')";
        ChanDatabase.insert(sql);
        
        matchs = getMatchs();
    }
    
    public static void updateMatch(Match match){
        LocalDate ld = chanapp.utilitaires.MesOutils.convertToLocalDate(match.getDateDebut());
        String sql = "UPDATE `matchs` SET `datedebut`='"+ld.toString()+"',`lieu`='"+match.getLieu()+"' WHERE `matchs`.`idmacth` = '"+match.getIdMatch()+"'";
        ChanDatabase.insert(sql);
        sql = "UPDATE `jouer` SET `idequipe1`='"+match.getIdEq1()+"',`idequipe2`='"+match.getIdEq2()+"' WHERE `idmacth`='"+match.getIdMatch()+"'";
        ChanDatabase.insert(sql);
        matchs = getMatchs();
    }
    
    public static ObservableList<Match> getMatchs(){
        
        matchs.clear();
    
        String sql = "SELECT * FROM matchs";
    
        ResultSet rs = ChanDatabase.select(sql);
        try {
            while (rs.next()) {            
                matchs.add(new Match(rs.getInt("idmacth"), rs.getInt("scoreequ1"), rs.getInt("scoreequ2"), rs.getDate("datedebut"), rs.getDate("datefin"), rs.getString("lieu")));
            }     
        } catch (Exception e) {
            System.out.println(e);
        }
        
        for (Match match : matchs) {
            sql = "SELECT * FROM jouer WHERE idmacth="+match.getIdMatch();
            rs = ChanDatabase.select(sql);
            try {
                while (rs.next()) {            
                    ResultSet rs1 = ChanDatabase.select("SELECT * FROM `equipes` WHERE idequipe="+rs.getInt("idequipe1"));
                    match.setIdEq1(rs.getInt("idequipe1"));
                    match.setIdEq2(rs.getInt("idequipe2"));
                    while (rs1.next()) {                        
                        match.setNomEquipe1(rs1.getString(3));
                    }
                    ResultSet rs0 = ChanDatabase.select("SELECT * FROM `equipes` WHERE idequipe="+rs.getInt("idequipe2"));
                    while (rs0.next()) {                        
                        match.setNomEquipe2(rs0.getString(3));
                    }
                }     
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    
        return matchs;
    
    } 
    
    public static void deleteMatch(int idMatch) {
        String sql = "DELETE FROM `matchs` WHERE `idmacth` = '"+idMatch+"'";
        ChanDatabase.insert(sql);
    }
    
    public void ajouterUneAction(){
    }
   
    public void demarrerChrono(){
    }
   
    public void arreterChrono(){
    }
   
    public void notifierFan(){ 
    }
    
    

    
    
}
