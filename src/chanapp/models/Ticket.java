/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chanapp.models;

/**
 *
 * @author Stanley
 */
public class Ticket {
    private float prix;
    private int numTicket;

    public Ticket(float prix, int numTicket) {
        this.prix = prix;
        this.numTicket = numTicket;
    }

    public int getNumTicket() {
        return numTicket;
    }

    public float getPrix() {
        return prix;
    }
    public int getIdTicket(){
    return 0;}

    public void setNumTicket(int numTicket) {
        this.numTicket = numTicket;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }
    
    
    
}
