/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chanapp.models;

import chanapp.ChanDatabase;
import java.sql.ResultSet;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;

/**
 *
 * @author Stanley
 * 
 */
public class Equipe {

   public static ObservableList<Equipe> equipes = FXCollections.observableArrayList();;



    
   private int idEquipe;
   private int idGroupe;
   private String nomEquipe;
   private int point;
   private Button consulter;

    public Equipe() {
    }

    public Equipe(int idEquipe, int idGroupe, String nomEquipe, int point) {
        this.idEquipe = idEquipe;
        this.idGroupe = idGroupe;
        this.nomEquipe = nomEquipe;
        this.point = point;
    }
    public Equipe(int idGroupe, String nomEquipe, int point) {
        this.idGroupe = idGroupe;
        this.nomEquipe = nomEquipe;
        this.point = point;
    }

    public void setConsulter(Button consulter) {
        this.consulter = consulter;
    }

    public Button getConsulter() {
        return consulter;
    }

    

    public int getIdEquipe() {
        return idEquipe;
    }

    public int getIdGroupe() {
        return idGroupe;
    }

    
    public String getNomEquipe() {
        return nomEquipe;
    }

    public static void creer(Equipe equipe){
        String sql = "INSERT INTO `equipes` (`idequipe`, `idgroupe`, `nomequipe`, `point`) VALUES (NULL, '"+equipe.getIdGroupe()+"', '"+ equipe.getNomEquipe()+"', '"+equipe.getPoint()+"')";
        ChanDatabase.insert(sql);
        
        equipes = getEquipes();
        
        getEquipes();
    }
    
    public int getPoint() {
        return point;
    }

    public void setNomEquipe(String nomEquipe) {
        this.nomEquipe = nomEquipe;
    }

    public void setPoint(int point) {
        this.point = point;
    }
    
    public void addJoueur(Joueur joueur){
       
    }
    
    public void deleteJoueur(int idJoueur){
    }
    
    public void addCoach(Coach coach){
    
    }
    
    public void deleteCoach(int idCoach){
    }
    
    public void setJoueur(Joueur joueur){
    }
    
    public Joueur[] getJoueur(){
        return null;
        
    }
    
    public static ObservableList<Equipe> getEquipes(){
        equipes.clear();
        
        ResultSet resultSet = ChanDatabase.select("SELECT * FROM `equipes`");
        
        try {
            while (resultSet.next()) {                
                equipes.add(new Equipe(resultSet.getInt("idequipe"), resultSet.getInt("idgroupe"), resultSet.getString("nomequipe"), resultSet.getInt("point")));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        
        return equipes;
    }
    
    public static void updateEquipe(Equipe equipe) {
        String sql = "UPDATE `equipes` SET `idgroupe`='"+equipe.getIdGroupe()+"',`nomequipe`='"+equipe.getNomEquipe()+"',`point`=0 WHERE `idequipe` = '"+equipe.getIdEquipe()+"'";
        ChanDatabase.insert(sql);    

    }
    public static void deleteEquipe(int idEquipe) {
        String sql = "DELETE FROM equipes WHERE idEquipe='"+idEquipe+"'";
        ChanDatabase.insert(sql);
    }
}
