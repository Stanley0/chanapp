/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chanapp.models;

import chanapp.ChanDatabase;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Stanley
 */
public class Joueur extends Personne{
    private int idjoueur;
    private int idEquipe;
    private String poste;
    private String status;
    private boolean titulaire;

    
    
    public Joueur(int idEquipe, String poste, String status, boolean titulaire, String nom, String prenom, int age, String nationalite) {
        super(nom, prenom, age, nationalite);
        this.idEquipe = idEquipe;
        this.poste = poste;
        this.status = status;
        this.titulaire = titulaire;
    }

    public Joueur(int idjoueur, int idEquipe, String poste, String status, boolean titulaire, String nom, String prenom, int age, String nationalite) {
        super(nom, prenom, age, nationalite);
        this.idjoueur = idjoueur;
        this.idEquipe = idEquipe;
        this.poste = poste;
        this.status = status;
        this.titulaire = titulaire;
    }
    
    
    public static void creer(Joueur joueur){
        int post = 0;
        if (joueur.isTitulaire()) {
            post=1;
        }
        String sql = "INSERT INTO `joueurs` (`idjoueur`, `post`, `statut`, `titulaire`, `idequipe`, `nom`, `prenom`, `age`, `nationalite`) "
                + "VALUES (NULL,'"+ joueur.getPoste() +"', '"+joueur.getStatus()+"', '"+post+"', '"+joueur.getIdEquipe()+"', '"
                +joueur.getNom()+"', '"+joueur.getPrenom()+"', '"+joueur.getAge()+"', '"+joueur.getNationalite()+"')";
        ChanDatabase.insert(sql);
    }
    
    public static ArrayList<Joueur> getJoueurs(int idEq){
        ArrayList<Joueur> joueurs = new ArrayList<>();
        String sql = "SELECT * FROM `joueurs` WHERE idequipe = '"+idEq+"'";
        ResultSet rs = ChanDatabase.select(sql);
        try {
            while (rs.next()) {
                boolean i = false;
                if (rs.getInt("titulaire") == 1) {
                    i = true;
                } 
                joueurs.add(new Joueur(rs.getInt("idjoueur"), rs.getInt("idequipe"), rs.getString("post"), rs.getString("statut"), i, rs.getString("nom"), rs.getString("prenom"), rs.getInt("age"), rs.getString("nationalite")));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return joueurs;
    }

    public int getIdEquipe() {
        return idEquipe;
    }

    public int getIdjoueur() {
        return idjoueur;
    }
    
    
    public String getPoste() {
        return poste;
    }

    public String getStatus() {
        return status;
    }

    public boolean isTitulaire() {
        return titulaire;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public void setTitulaire(boolean titulaire) {
        this.titulaire = titulaire;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getNbrButTotal(){
        return 0;
    }
    
    
    public int getNbrPasseTotal(){
        return 0;
    }
    public int getNbrFauteTotal(){
        return 0;
    }
}
