/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chanapp.models;

import chanapp.ChanDatabase;
import java.sql.ResultSet;

/**
 *
 * @author Stanley
 */
public class Coach extends Personne {
    private int idCoach;
    public Coach(String nom,String prenom,int age,String nationalite) {
        super(nom, prenom, age, nationalite);
    }

    public Coach(int idCoach, String nom, String prenom, int age, String nationalite) {
        super(nom, prenom, age, nationalite);
        this.idCoach = idCoach;
    }

    public Coach() {
        super(null, null, 0, null);
    }
    
    
    public int getidCoach (){
        return 0;
    }

    public static void creer(Coach coach){
        String sql = "INSERT INTO `coachs` (`idcoach`, `nom`, `prenom`, `age`, `nationalite`) "
                + "VALUES (NULL, '"+coach.getNom()+"', '"+coach.getPrenom()+"', '"+coach.getAge()+"', '"+coach.getNationalite()+"')";
        ChanDatabase.insert(sql);
    }
    public static Coach getCoach(int idcoach){
        Coach coach = null;
        String sql = "SELECT * FROM `coachs` WHERE idcoach = '"+idcoach+"'";
        ResultSet rs = ChanDatabase.select(sql);
        try {
            while (rs.next()) {
                coach = new Coach(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5));            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return coach;
    }
    
}
