/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chanapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Stanley
 */
public class ChanDatabase {
    static PreparedStatement pst;
    static Connection con;
    
    public static void driver(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("driver ok");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public static void connexion(){
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/chan?zeroDateTimeBehavior=convertToNull", "root", "");
            System.out.println("connexion ok");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public static void insert(String sql){
        try {
            pst = con.prepareStatement(sql);
            pst.execute();
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
    
    public static ResultSet select(String sql){
        
        ResultSet resultSet = null ;
        
        try {
            pst = con.prepareStatement(sql);
            resultSet = pst.executeQuery();
            
            
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        
        return resultSet;
    
    }
}
